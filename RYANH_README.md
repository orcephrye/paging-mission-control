Hello and thank you for your time and consideration.

I had to make at least one assumption when following this programming challenge.

Normally when reviewing requirements and a question comes up I would go back and attempt to get clarification but 
the PDF didn't mention that.

The main assumption I made was regarding what entry needed to be sent to the output when in alert. At first, I thought
that the ask was for the entry that triggers the alert. So the latest alerting entry and all subsequent entries that 
are in alert. However, upon reviewing the example output in the README file the output is of the first occurrence. There
is no mention of this in the README. So going off the example output I made the assumption that this should output the 
first occurrence of every instance of a new alert. Alert being defined by the two parameters noted in the README. 

I also left some things hardcoded or otherwise less flexible than it could be. I did not want to overcomplicate the 
solution which I believe to be in the spirit of this code challenge.