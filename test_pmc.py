import pytest
from datetime import timedelta
from dateutil.parser import parse
import pmc
from pmc.AlertLog import AlertLog
from pmc.TimedLog import TimeLog
from pmc.LogReader import LogReader


def test_timed_log_three_entries():
    entry_str = """20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
    20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
    20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT"""

    latest = parse('20180101 23:01:26.011')
    oldest = parse('20180101 23:01:05.001')

    tl = TimeLog(timedelta(minutes=5))
    for entry in entry_str.splitlines():
        tl.put(entry.strip().split('|'))

    assert latest == tl.latest_entry['timestamp']
    assert oldest == tl.oldest_entry['timestamp']
    assert tl.size == 3


def test_timed_log_large_gap():
    entry_str = """20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
    20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
    20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
    20180101 23:50:38.001|1000|101|98|25|20|102.9|TSTAT"""

    latest = parse('20180101 23:50:38.001')
    tl = TimeLog(timedelta(minutes=5))
    for entry in entry_str.splitlines():
        tl.put(entry.strip().split('|'))

    assert latest == tl.latest_entry['timestamp']
    assert tl.latest_entry['timestamp'] == tl.oldest_entry['timestamp']
    assert tl.size == 1


def test_timed_log_single_entry():
    entry_str = """20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT"""

    latest = parse('20180101 23:01:05.001')
    tl = TimeLog(timedelta(minutes=5))
    for entry in entry_str.splitlines():
        tl.put(entry.strip().split('|'))

    assert latest == tl.latest_entry['timestamp']
    assert tl.latest_entry['timestamp'] == tl.oldest_entry['timestamp']
    assert tl.size == 1


def test_timed_log_pop_all():
    entry_str = """20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
    20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
    20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT"""

    tl = TimeLog(timedelta(minutes=5))
    for entry in entry_str.splitlines():
        tl.put(entry.strip().split('|'))

    for _ in range(tl.size):
        tl.pop()

    assert tl.latest_entry is None
    assert tl.oldest_entry is None
    assert tl.size == 0


def test_timed_log_one_min():
    entry_str = """20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
                20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
                20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
                20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
                20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
                20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
                20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT
                20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT
                20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
                20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
                20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT
                20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT
                20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
                20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT"""

    latest = parse('20180101 23:05:07.421')
    oldest = parse('20180101 23:04:11.531')

    tl = TimeLog(timedelta(minutes=1))
    for entry in entry_str.splitlines():
        tl.put(entry.strip().split('|'))

    assert latest == tl.latest_entry['timestamp']
    assert oldest == tl.oldest_entry['timestamp']
    assert tl.size == 3


def test_timed_log_four_min():
    entry_str = """20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
                20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
                20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
                20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
                20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
                20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
                20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT
                20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT
                20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
                20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
                20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT
                20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT
                20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
                20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT"""

    latest = parse('20180101 23:05:07.421')
    oldest = parse('20180101 23:01:09.521')

    tl = TimeLog(timedelta(minutes=4))
    for entry in entry_str.splitlines():
        tl.put(entry.strip().split('|'))

    assert latest == tl.latest_entry['timestamp']
    assert oldest == tl.oldest_entry['timestamp']
    assert tl.size == 13


def test_timed_log_five_min():
    entry_str = """20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
                20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
                20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
                20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
                20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
                20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
                20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT
                20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT
                20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
                20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
                20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT
                20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT
                20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
                20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT"""

    latest = parse('20180101 23:05:07.421')
    oldest = parse('20180101 23:01:05.001')

    tl = TimeLog(timedelta(minutes=5))
    for entry in entry_str.splitlines():
        tl.put(entry.strip().split('|'))

    assert latest == tl.latest_entry['timestamp']
    assert oldest == tl.oldest_entry['timestamp']
    assert tl.size == 14


def test_alert_log_batt():
    entry_str = """20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
    20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT
    20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT"""

    alert_timestamp = parse('20180101 23:01:09.521')

    al = AlertLog(timedelta(minutes=5), 'red-low-limit', 'raw-value', 3, 'lt')
    for entry in entry_str.splitlines():
        al.put(entry.strip().split('|'))

    assert al.occurrences == 3
    assert al.isOnGoing is False
    assert al.inAlert is True
    assert al.get_first_occurrence()['timestamp'] == alert_timestamp


def test_alert_log_tstat():
    entry_str = """20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
    20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
    20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT"""

    alert_timestamp = parse('20180101 23:01:38.001')

    al = AlertLog(timedelta(minutes=5), 'red-high-limit', 'raw-value', 3, 'gt')
    for entry in entry_str.splitlines():
        al.put(entry.strip().split('|'))

    assert al.occurrences == 3
    assert al.isOnGoing is False
    assert al.inAlert is True
    assert al.get_first_occurrence().get('timestamp') == alert_timestamp


def test_alert_log_no_alerts():
    entry_str = """20180101 23:01:38.001|1000|101|98|25|20|100.9|TSTAT
    20180101 23:03:03.008|1000|101|98|25|20|100.7|TSTAT
    20180101 23:03:05.009|1000|101|98|25|20|100.2|TSTAT"""

    al = AlertLog(timedelta(minutes=5), 'red-high-limit', 'raw-value', 3, 'gt')
    for entry in entry_str.splitlines():
        al.put(entry.strip().split('|'))

    assert al.occurrences == 0
    assert al.isOnGoing is False
    assert al.inAlert is False
    assert al.get_first_occurrence().get('timestamp', None) is None


def test_alert_log_ongoing_alerts():
    entry_str = """20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
        20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
        20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
        20180101 23:04:05.009|1000|101|98|25|20|101.2|TSTAT"""

    alert_timestamp = parse('20180101 23:01:38.001')

    al = AlertLog(timedelta(minutes=5), 'red-high-limit', 'raw-value', 3, 'gt')
    for entry in entry_str.splitlines():
        al.put(entry.strip().split('|'))

    assert al.occurrences == 4
    assert al.isOnGoing is True
    assert al.inAlert is True
    assert al.get_first_occurrence().get('timestamp', None) == alert_timestamp


def test_alert_log_clear_alert():
    entry_str = """20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
        20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
        20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
        20180101 23:04:05.009|1000|101|98|25|20|100.2|TSTAT
        20180101 23:05:05.009|1000|101|98|25|20|100.2|TSTAT
        20180101 23:06:39.009|1000|101|98|25|20|100.2|TSTAT"""

    alert_timestamp = parse('20180101 23:03:03.008')

    al = AlertLog(timedelta(minutes=5), 'red-high-limit', 'raw-value', 3, 'gt')
    for entry in entry_str.splitlines():
        al.put(entry.strip().split('|'))

    assert al.occurrences == 2
    assert al.isOnGoing is False
    assert al.inAlert is False
    assert al.get_first_occurrence().get('timestamp', None) == alert_timestamp


def test_alert_log_multiple_alerts():
    entry_str = """20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
    20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
    20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
    20180101 23:11:38.001|1000|101|98|25|20|102.9|TSTAT
    20180101 23:13:03.008|1000|101|98|25|20|102.7|TSTAT
    20180101 23:13:05.009|1000|101|98|25|20|101.2|TSTAT"""

    alert_timestamp_1 = parse('20180101 23:01:38.001')
    alert_timestamp_2 = parse('20180101 23:11:38.001')

    al = AlertLog(timedelta(minutes=5), 'red-high-limit', 'raw-value', 3, 'gt')
    entry_list = entry_str.splitlines()
    for index in range(3):
        al.put(entry_list[index].strip().split('|'))

    assert al.occurrences == 3
    assert al.isOnGoing is False
    assert al.inAlert is True
    assert al.get_first_occurrence().get('timestamp') == alert_timestamp_1

    al.put(entry_list[3].strip().split('|'))

    assert al.occurrences == 1
    assert al.isOnGoing is False
    assert al.inAlert is False
    assert al.get_first_occurrence().get('timestamp') == alert_timestamp_2

    al.put(entry_list[4].strip().split('|'))
    al.put(entry_list[5].strip().split('|'))

    assert al.occurrences == 3
    assert al.isOnGoing is False
    assert al.inAlert is True
    assert al.get_first_occurrence().get('timestamp') == alert_timestamp_2


def test_log_reader_single_alerts():

    test_alert_list = [
        {
            'satelliteId': 1000,
            'severity': 'RED HIGH',
            'component': 'TSTAT',
            'timestamp': '2018-01-01 23:01:38.001000'},
        {
            'satelliteId': 1000,
            'severity': 'RED LOW',
            'component': 'BATT',
            'timestamp': '2018-01-01 23:01:09.521000'}
    ]

    lr = LogReader('test_data.txt')
    lr.read_logs()

    assert test_alert_list == lr.alerts_list


def test_log_reader_multiple_alerts():

    test_alert_list = [
          {
            "satelliteId": 1000,
            "severity": "RED HIGH",
            "component": "TSTAT",
            "timestamp": "2018-01-01 23:01:38.001000"
          },
          {
            "satelliteId": 1000,
            "severity": "RED LOW",
            "component": "BATT",
            "timestamp": "2018-01-01 23:01:09.521000"
          },
          {
            "satelliteId": 1000,
            "severity": "RED HIGH",
            "component": "TSTAT",
            "timestamp": "2018-01-01 23:11:38.001000"
          },
          {
            "satelliteId": 1000,
            "severity": "RED LOW",
            "component": "BATT",
            "timestamp": "2018-01-01 23:11:09.521000"
          }
        ]

    lr = LogReader('test_data_2.txt')
    lr.read_logs()

    assert test_alert_list == lr.alerts_list
