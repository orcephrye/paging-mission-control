import sys
from LogReader import LogReader


def main():
    if len(sys.argv) < 2:
        print("Please provide a log file to parse")
        sys.exit(-1)
    try:
        lr = LogReader(sys.argv[1])
        lr.read_logs()
    except Exception as e:
        print(f'An exception occurred closing. \nError: {e}')


if __name__ == '__main__':
    main()
