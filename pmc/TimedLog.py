from dateutil.parser import parse, ParserError
from datetime import datetime, timedelta
from collections import deque
from typing import Optional
import logging


log = logging.getLogger('TimeLog')


class TimeLog:

    __entry_keys = ('timestamp', 'satelliteId', 'red-high-limit', 'yellow-high-limit',
                    'yellow-low-limit', 'red-low-limit', 'raw-value', 'component')

    def __init__(self, holding_time: timedelta):
        """
            This is a simple class that wraps a deque with the goal of managing how many entries are in the deque based
            on their timestamp.
        :param holding_time: (timedelta) The max length of time between the oldest and latest entry.
        """
        self.__wrapped_queue = deque()
        self.__size = 0
        self.oldest_entry = None
        self.latest_entry = None
        self.holding_time = holding_time

    def __str__(self) -> str:
        return str([i for i in self.__wrapped_queue])

    def put(self, new_entry: list) -> None:
        """
            Puts an entry into the deque. Also adjusts the size and the latest/oldest entry values.
        :param new_entry: (list) A list of strings that has been read in from a log file.
        :return:
        """
        entry_dict = self._parse_entry(new_entry)
        self.__wrapped_queue.append(entry_dict)
        self.__size += 1
        self.latest_entry = entry_dict
        if self.oldest_entry is None:
            self.oldest_entry = self.__wrapped_queue[0]

    def pop(self) -> Optional[dict]:
        """
            This pops an entry from the deque using 'popleft'. It also manages size and oldest/latest entry.
        :return: (dict) The entry that has just been popped.
        """
        if self.__size > 0:
            old_entry = self.__wrapped_queue.popleft()
            self.__size -= 1
            if self.__size == 0:
                self.oldest_entry = None
                self.latest_entry = None
            else:
                self.oldest_entry = self.__wrapped_queue[0]
            return old_entry
        else:
            log.debug('Attempted to pop an empty queue!')
        return None

    def _parse_entry(self, entry_list: list) -> dict:
        if len(entry_list) != 8:
            log.error(f"Error: The entry provided {entry_list} isn't the correct size for parsing")
            raise Exception(f"Error: The entry provided {entry_list} isn't the correct size for parsing")
        entry_dict = dict(zip(self.__entry_keys, entry_list))
        entry_dict['timestamp'] = self._parse_timestamp(entry_list[0])
        while self._compare_timestamp(entry_dict['timestamp']):
            self.pop()
        return entry_dict

    def _compare_timestamp(self, entry_timestamp: datetime) -> bool:
        if self.oldest_entry:
            return entry_timestamp > self.oldest_entry['timestamp'] + self.holding_time
        return False

    @staticmethod
    def _parse_timestamp(entry_timestamp: str) -> datetime:
        try:
            dt = parse(entry_timestamp)
        except ParserError as e:
            log.error(f'Parse of {entry_timestamp} failed for reason: {e}')
            raise e
        else:
            return dt

    @property
    def size(self):
        return self.__size


if __name__ == '__main__':
    print("This should be called as a module.")
