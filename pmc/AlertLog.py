from TimedLog import TimeLog, timedelta
import logging


log = logging.getLogger('AlertLog')


class AlertLog(TimeLog):

    approved_comparisons = ('gt', 'gte', 'lt', 'lte')

    def __init__(self, holding_time: timedelta,
                 alert_key: str,
                 threshold_key: str,
                 occurrence_threshold: int,
                 comparison: str):
        super().__init__(holding_time)
        self.alert_key = alert_key
        self.threshold_key = threshold_key
        self.occurrence_threshold = occurrence_threshold
        if comparison not in self.approved_comparisons:
            raise Exception(f"Incorrect comparison: {comparison} must in {self.approved_comparisons}")
        self.comparison = comparison
        self.__alert_entry_list = []
        self.__ongoing = False

    def put(self, new_entry: list) -> None:
        """
            This wraps the put method of the TimedLog class. Once complete it checks the latest_entry value to see
            if the raw value is violating its threshold. If it is it increases the occurrences value.
        :param new_entry: (list)
        :return:
        """
        super().put(new_entry)
        if self._process_alert(self.latest_entry):
            self.__alert_entry_list.append(self.latest_entry)

    def pop(self) -> dict:
        """
            This wraps the pop method of the TimedLog class. Once complete it checks the old_entry that was popped to
            see if the raw value violated the threshold. If it did then it decreases the occurrences because that
            entry is no longer in the TimedLog.
        :return: (dict) the popped entry
        """
        old_entry = super().pop()
        if old_entry and self._process_alert(old_entry):
            try:
                self.__alert_entry_list.remove(old_entry)
            except ValueError as e:
                log.error(f'The old_entry was not in the alert_entry_list. This should never happen as it implies that'
                          f' the list was manipulated or that the values determining alert threshold where manipulated'
                          f' during runtime.')
                raise e
            finally:
                if not self.inAlert:
                    self.__ongoing = False
        return old_entry

    def get_first_occurrence(self) -> dict:
        if self.__alert_entry_list:
            return self.__alert_entry_list[0]
        return dict()

    def _process_alert(self, entry: dict) -> bool:
        try:
            alert_val = float(entry[self.alert_key])
            threshold_val = float(entry[self.threshold_key])
        except Exception as e:
            log.error(f'Failed to parse keys from entry: {e}')
            raise e
        else:
            if self.comparison == 'gt':
                return threshold_val > alert_val
            elif self.comparison == 'gte':
                return threshold_val >= alert_val
            elif self.comparison == 'lt':
                return threshold_val < alert_val
            else:
                return threshold_val <= alert_val

    @property
    def occurrences(self):
        return len(self.__alert_entry_list)

    @property
    def inAlert(self) -> bool:
        """
            Checks to see if the number of occurrences are greater than or equal to the occurrence threshold.
        :return: (bool)
        """
        return self.occurrences >= self.occurrence_threshold

    @property
    def isOnGoing(self) -> bool:
        """
            This checks if this is an active alert that is still ongoing and not a new alert that just happened.
        :return:
        """
        if self.occurrences > self.occurrence_threshold:
            self.__ongoing = True
        return self.__ongoing


if __name__ == '__main__':
    print("This should be called as a module.")
