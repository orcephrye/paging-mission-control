from typing import Tuple
from datetime import timedelta
import json
import logging
from AlertLog import AlertLog


log = logging.getLogger('LogReader')


class LogReader:

    __default_alert_args_map: dict = {'BATT': (timedelta(minutes=5), 'red-low-limit', 'raw-value', 3, 'lt'),
                                      'TSTAT': (timedelta(minutes=5), 'red-high-limit', 'raw-value', 3, 'gt')}
    __default_delimiter: str = '|'
    __default_uniq_vals: tuple = (1, 7)

    def __init__(self, logFile: str, delimiter: str = None, uniq_vals: tuple = None, alert_args_map: dict = None):
        """
            This class reads a provided log file. Split log up depending on unique values.
        :param logFile: (str) A path to a file that will be read in by this class
        :param delimiter: (str) This will be used to 'split' each line of the logfile as it is being read in.
        :param uniq_vals: (tuple) This is tuple of index values that are used to generate another tuple were values
            correspond with the indices. This is used as a key in a dictionary were its values are the AlertLog classes
            that take in entries associated with the combination of unique values.
        :param alert_args_map: (Dict) This is a dictionary were its keys are strings that are the component types and
            values are the arguments used to make an AlertLog instance.
        """
        self.logFile = logFile
        self.delimiter = self.__default_delimiter if delimiter is None else delimiter
        self.uniq_vals = self.__default_uniq_vals if uniq_vals is None else uniq_vals
        self.alert_args_map = self.__default_alert_args_map if alert_args_map is None else alert_args_map
        self.log_map = dict()
        self.alerts_list = []

    def read_logs(self) -> None:
        """
            This uses a generator to read line by line the log file, parse each line, separate out to different AlertLog
            instances based on the satelliteId and component keys using the uniq_vals class obj variable and then checks
            if the associated AlertLog is actually in alert. If it is it gets printed.
        :return:
        """
        for line in LogReader.log_generator(self.logFile):
            entry, uniq_tup = self._parse(line)
            if not entry or not uniq_tup:
                continue
            self._insert_into_alertlog(entry, uniq_tup)
            self._is_in_alert(uniq_tup)
        print(json.dumps(self.alerts_list, indent=2))

    def _is_in_alert(self, uniq_tup: tuple) -> None:
        if uniq_tup not in self.log_map:
            return
        if self.log_map[uniq_tup].inAlert and not self.log_map[uniq_tup].isOnGoing:
            try:
                first_alert_entry = self.log_map[uniq_tup].get_first_occurrence()
                printDict = {'satelliteId': int(first_alert_entry['satelliteId']),
                             'severity':
                                 self.log_map[uniq_tup].alert_key.replace('-', ' ').strip('limit').strip().upper(),
                             'component': first_alert_entry['component'],
                             'timestamp': str(first_alert_entry['timestamp'])}
                self.alerts_list.append(printDict)
            except Exception as e:
                log.debug(f'DEBUG: There was a failure producing the output: \n{e}')

    def _insert_into_alertlog(self, entry, uniq_tup) -> None:
        if uniq_tup not in self.log_map:
            arguments = self.alert_args_map.get(uniq_tup[1])
            if not arguments:
                raise Exception(f"No arguments provided for component: {uniq_tup[1]}")
            if type(arguments) is dict:
                self.log_map[uniq_tup] = AlertLog(**arguments)
            elif type(arguments) in (tuple, list):
                self.log_map[uniq_tup] = AlertLog(*arguments)
            else:
                raise Exception(f'Arguments provided are not formatted correctly use: dict, tuple or list')
        self.log_map[uniq_tup].put(entry)

    def _parse(self, line: str) -> Tuple[list, tuple]:
        try:
            lineList = line.split(self.delimiter)
            uniq_list = [lineList[i] for i in self.uniq_vals]
        except Exception as e:
            log.debug(f'Failed too parse line: {line}\nERROR: {e}')
            return [], ()
        else:
            return lineList, tuple(uniq_list)

    @staticmethod
    def log_generator(logFile: str):
        with open(logFile, 'r') as f:
            while True:
                line = f.readline()
                if not line:
                    break
                yield line.strip()


if __name__ == '__main__':
    print("This should be called as a module.")
